// Tạo biến 
// Nên dùng const để khóa data
const DSNV = 'Danh Sách Nhân Viên';
// Biến chức
const giamDoc = 'Giám đốc';
const truongPhong = 'Trưởng phòng';
const nhanVien = 'Nhân viên';
// Biến cấp độ
const nvXuatSac = 'Xuất sắc';
const nvGioi = 'Giỏi';
const nvKha = 'Khá';
const nvTrungBinh = 'Trung bình';
// main array => đừng để sai đừng quên [].
var danhSachNhanVien = [];
// class objects
function NhanVien(_taikhoan,_hoten,_email,_matkhau,_ngaylam,_luongcoban,_chucvu,_giolam){
    this.taikhoan = _taikhoan;
    this.hoten = _hoten;
    this.email = _email;
    this.matkhau = _matkhau;
    this.ngaylam = _ngaylam;
    this.luongcoban = _luongcoban;
    this.chucvu = _chucvu;
    this.giolam = _giolam;
    this.TongLuong = function(){
        if (this.chucvu == giamDoc){
            return (this.luongcoban * 3).toLocaleString();
        }else if (this.chucvu == truongPhong){
            return (this.luongcoban * 2).toLocaleString();
        }else{
            return this.luongcoban.toLocaleString();
        }
    }
    this.XepLoai = function(){
        if (this.giolam >= 192){
            return nvXuatSac;
        }else if (this.giolam >= 176){
            return nvGioi;
        }else if (this.giolam >= 160){
            return nvKha;
        }else{
            return nvTrungBinh;
        }
    }
}

// Tạo lưu local Json
function saveLocalStorage() {
    let jsonDanhSachNV = JSON.stringify(danhSachNhanVien);
    console.log('jsonDanhSachNV',jsonDanhSachNV);
    localStorage.setItem(DSNV, jsonDanhSachNV);
}
// load Json
var dataJson = localStorage.getItem(DSNV);
if (dataJson !== null) {
  var arrayTemp = JSON.parse(dataJson);

  // vì localStorage không lưu function điểm trung bình (dtb) nên phải convert data thông qua array tạm (bro Anh Trần chỉ)
  for (var i = 0; i < arrayTemp.length; i++) {
    var currentNVTemp = arrayTemp[i];
    var nvSaved = new NhanVien(
      currentNVTemp.taikhoan,
      currentNVTemp.hoten,
      currentNVTemp.email,
      currentNVTemp.matkhau,
      currentNVTemp.ngaylam,
      currentNVTemp.luongcoban,
      currentNVTemp.chucvu,
      currentNVTemp.giolam,
    );
    danhSachNhanVien.push(nvSaved);
    console.log('danhSachNhanVien',danhSachNhanVien);
  }
  renderDanhSachNhanVien(danhSachNhanVien);
}

// CRUD
// Create Read Update Delete
// Nên tạo 2 Js riêng 1 cái controller và 1 cái validation
// Controller Js sẽ tạo các funtion lấy trường dữ liệu từ các khung điền 
// Validation là JS bước xét validate các dữ liệu input có đúng không.

function themNhanVien(){
    var newNV = layThongTinTuForm();
    console.log('newNV',newNV);

    var isValid = true;
    // check seperate ID
    isValid =
    checkNull(newNV.taikhoan, "tbTKNV", "Mã nhân viên không được để trống") &&
    checkDuplication(newNV.taikhoan, danhSachNhanVien) &&
    checkInputNumber(newNV.taikhoan, 'tbTKNV') &&
    checkInputLength(newNV.taikhoan,4,6,'tbTKNV');
    // check seperate name (always check null first)
    isValid = isValid &
    (checkNull(newNV.hoten, 'tbTen', 'Tên gì đại ca ?') &&
    // checkInputText(newNV.hoten, 'tbTen'));
    checkInputNameWithUnicode(newNV.hoten, 'tbTen'));
    // check seperate email (always check null first)
    isValid = isValid &
    (checkNull(newNV.email, 'tbEmail', 'Email đâu cha nội ?') &&
    checkInputEmail(newNV.email, 'tbEmail'));
    // check seperate password (always check null first)
    isValid = isValid &
    (checkNull(newNV.matkhau, 'tbMatKhau', 'Mật khẩu ? Liều à ?') &&
    checkInputPassword(newNV.matkhau, 'tbMatKhau'));
    // check seperate day work (always check null first)
    isValid = isValid &
    (checkNull(newNV.ngaylam, 'tbNgay', 'Làm được nhiu buổi ông thần ?') &&
    checkInputDate(newNV.ngaylam, 'tbNgay'));
    // check seperate salary (always check null first)
    isValid = isValid &
    (checkNull(newNV.luongcoban, 'tbLuongCB', 'Tính làm không lương cho xã hội à ?') &&
    checkInputValue(newNV.luongcoban,1000000,20000000,'tbLuongCB'));
    // check seperate work hour (always check null first)
    isValid = isValid &
    (checkNull(newNV.giolam, 'tbGiolam', 'Giờ làm không được để trống!') &&
    checkInputValue(newNV.giolam,80,200,'tbGiolam'));
    // last check element with single check only
    isValid = isValid &
    checkNull(newNV.chucvu, 'tbChucVu', 'Chức gì cha lội?');

    if (isValid){
        danhSachNhanVien.push(newNV);
        console.log('danhSachNhanVien',danhSachNhanVien);
        saveLocalStorage();
        renderDanhSachNhanVien(danhSachNhanVien);
    }else{
        // querySelectorAll on selector is an array, make loop to cover all
        const showError = document.querySelectorAll(".modal-body .form-group .sp-thongbao");
        for (let i = 0; i < showError.length; i++) {
        showError[i].style.display = 'block';
        }
    }
}

function xoaNhanVien(id){
    var viTri = viTriNhanVien(id, danhSachNhanVien);
    console.log("Vị trí đã xóa:", viTri);
    if (viTri !== -1){
        danhSachNhanVien.splice(viTri, 1);
        saveLocalStorage();
        renderDanhSachNhanVien(danhSachNhanVien);
    }
}

function suaNhanVien(id){
    var viTri = viTriNhanVien(id, danhSachNhanVien);
    if (viTri == -1) return;
    var nvEdit = danhSachNhanVien[viTri];
    console.log('Data cần edit: ', nvEdit);
    showThongTinLenForm(nvEdit);
    document.getElementById("tknv").disabled = true;
}

function updateNhanVien(){
    var dataEdited = layThongTinTuForm();

    var isValid = true;
    // check seperate name (always check null first)
    isValid = 
    (checkNull(dataEdited.hoten, 'tbTen', 'Tên gì đại ca ?') &&
    // checkInputText(newNV.hoten, 'tbTen'));
    checkInputNameWithUnicode(dataEdited.hoten, 'tbTen'));
    // check seperate email (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.email, 'tbEmail', 'Email không được để trống!') &&
    checkInputEmail(dataEdited.email, 'tbEmail'));
    // check seperate password (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.matkhau, 'tbMatKhau', 'Mật khẩu đâu sếp ?') &&
    checkInputPassword(dataEdited.matkhau, 'tbMatKhau'));
    // check seperate day work (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.ngaylam, 'tbNgay', 'Ngày làm không được để trống ông thần ơi') &&
    checkInputDate(dataEdited.ngaylam, 'tbNgay'));
    // check seperate salary (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.luongcoban, 'tbLuongCB', 'Lương đâu thím ?') &&
    checkInputValue(dataEdited.luongcoban,1000000,20000000,'tbLuongCB'));
    // check seperate work hour (always check null first)
    isValid = isValid &
    (checkNull(dataEdited.giolam, 'tbGiolam', 'Đù không đi làm à ? Giờ làm đâu ?') &&
    checkInputValue(dataEdited.giolam,80,200,'tbGiolam'));
    // last check element with single check only
    isValid = isValid &
    checkNull(dataEdited.chucvu, 'tbChucVu', 'Ngồi ghế gì ?');

    if (isValid){
        var viTri = viTriNhanVien(dataEdited.taikhoan, danhSachNhanVien);
        if (viTri == -1) return;
        danhSachNhanVien[viTri] = dataEdited;
        renderDanhSachNhanVien(danhSachNhanVien);
        saveLocalStorage();
        document.getElementById("tknv").disabled = false;
    }else{
        // querySelectorAll on selector is an array, make loop to cover all
        const showError = document.querySelectorAll(".modal-body .form-group .sp-thongbao");
        for (let i = 0; i < showError.length; i++) {
        showError[i].style.display = 'block';
        }
    }
}

function showID(){
    document.getElementById("tknv").disabled = false;
}

function timKiemNhanVien(){
    // xác định giá trị tìm kiếm
    var thongTinCanTim = document.getElementById('searchName').value;
    // phân tích từ khóa tìm bằng phương thức chuyển hóa từ khóa thành kết quả muốn tìm
    var thongTinParsed = parseSearchValue(thongTinCanTim);
    // chuyển về lowerCase để filter & trả kết quả
    var thongTin = thongTinParsed.toLowerCase();
    console.log('thongTin tìm ra: ', thongTin);
    // dùng hàm filter kết hợp indexOf để lọc thông tin
    var danhSachNhanVienFiltered = danhSachNhanVien.filter(element => element.XepLoai().toLowerCase().indexOf(thongTin) > -1);
    renderDanhSachNhanVien(danhSachNhanVienFiltered);
}

